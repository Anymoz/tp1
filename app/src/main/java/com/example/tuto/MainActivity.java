package com.example.tuto;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class MainActivity extends AppCompatActivity {
        Toolbar toolbar;
        ListView listView;



    String[] Animale_Names = AnimalList.getNameArray();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
            toolbar =  (Toolbar) findViewById(R.id.toolbar);
            toolbar.setTitle("TP1");
            listView = (ListView) findViewById(R.id.listView);

        Myadapter myadapter = new Myadapter(MainActivity.this,Animale_Names);
                 listView.setAdapter(myadapter);
                listView.setOnItemClickListener( new AdapterView.OnItemClickListener(){

                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
                    Intent intent =  new Intent(MainActivity.this, SecondActivity.class);
                        intent.putExtra("animale name", Animale_Names[i]
                            );
                    startActivity(intent);

                }
            });


    }
}
