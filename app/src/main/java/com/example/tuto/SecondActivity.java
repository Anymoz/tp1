package com.example.tuto;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.textfield.TextInputEditText;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Toolbar toolbarm = (Toolbar) findViewById(R.id.toolbar1);
        ImageView imgView = (ImageView) findViewById(R.id.imageView);
        TextView name_txt = (TextView) findViewById(R.id.name_txt);
        final TextInputEditText max_vie = (TextInputEditText) findViewById(R.id.max_vie) ;
        final TextInputEditText gestation = (TextInputEditText) findViewById(R.id.gestation) ;
        final TextInputEditText naissance = (TextInputEditText) findViewById(R.id.naissance) ;
        final TextInputEditText poids = (TextInputEditText) findViewById(R.id.Poids) ;
        final TextInputEditText status = (TextInputEditText) findViewById(R.id.status) ;
        Button save = (Button) findViewById(R.id.button);

        Bundle bundle = getIntent().getExtras();
            if(bundle != null){
                 final Animal animal = AnimalList.getAnimal(bundle.getString("animale name"));
                toolbarm.setTitle(bundle.getString("animale name"));
                name_txt.setText(bundle.getString("animale name"));
                imgView.setImageResource(getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName()));
                max_vie.setText(animal.getStrHightestLifespan());
                gestation.setText(animal.getStrGestationPeriod());
                naissance.setText(animal.getStrBirthWeight());
                poids.setText(animal.getStrAdultWeight());
                status.setText( animal.getConservationStatus());
                max_vie.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                poids.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                naissance.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                gestation.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
                save.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        if( !TextUtils.isDigitsOnly(max_vie.getText()) || !TextUtils.isDigitsOnly(poids.getText())
                        || !TextUtils.isDigitsOnly(naissance.getText()) || !TextUtils.isDigitsOnly(gestation.getText())
                        || max_vie.getText().length()==0 || poids.getText().length()==0 || naissance.getText().length()==0
                                || gestation.getText().length()==0 || status.getText().length()==0){
                            Toast.makeText(getApplicationContext(), "Saisir des entiers !", Toast.LENGTH_LONG).show();
                        }
                        else {
                            animal.setHightestLifespan(Integer.parseInt(max_vie.getText().toString()));
                            animal.setAdultWeight(Integer.parseInt(poids.getText().toString()));
                            animal.setBirthWeight(Integer.parseInt(naissance.getText().toString()));
                            animal.setConservationStatus(status.getText().toString());
                            animal.setGestationPeriod(Integer.parseInt(gestation.getText().toString()));
                            Intent intent = new Intent(SecondActivity.this, MainActivity.class);

                            Context context = getApplicationContext();
                            CharSequence text = "Informations enregistrés !";
                            int duration = Toast.LENGTH_SHORT;

                            Toast toast = Toast.makeText(context, text, duration);
                            toast.show();
                            startActivity(intent);
                        }
                    }
                });
            }



    }
}
